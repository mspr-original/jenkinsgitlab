# Utilisez l'image de base Python
FROM python:3.8-slim

# Définissez le répertoire de travail dans le conteneur
WORKDIR /app

# Copiez le fichier requirements.txt pour installer les dépendances
COPY requirements.txt .

# Installez nmap
RUN apt-get update && apt-get install -y nmap 
RUN apt-get install iputils-ping -y
# Installez les dépendances
RUN pip install --no-cache-dir -r requirements.txt

# Copiez les fichiers dans le répertoire de travail
COPY . .

# Exposez le port 5000 pour Flask
EXPOSE 5000

# Commande pour exécuter l'application Flask
CMD ["python", "app.py"]
