from flask import Flask, render_template ,request
import subprocess
import re
import socket
import os
import psutil
import nmap


print("PATH:", os.environ['PATH'])

app = Flask(__name__, template_folder='index.html')

@app.route('/', methods=['GET', 'POST'])

def index():
    local_ip = None
    vm_name = None
    count_machine = None
    rtt = None
    complet=[]
    if request.method == 'GET':
        try:
            # Obtenir la liste des interfaces réseau
            interfaces = psutil.net_if_addrs()

            # Rechercher une interface avec une adresse IP non locale
            for interface, addresses in interfaces.items():
                for address in addresses:
                    if address.family == socket.AF_INET and not address.address.startswith("127.") and not address.address.startswith("172."):
                        local_ip = address.address            

        except Exception as e:
            print("Une erreur s'est produite :", str(e))
            local_ip = "ERROR"

        try:
            # Obtenir le nom d'hôte de la machine
            vm_name = socket.gethostname()
            print(vm_name)
        except Exception as e:
            print("Une erreur s'est produite :", str(e))
            vm_name = "ERROR"
        
    
            
        try:
            nm = nmap.PortScanner()
            nm.scan(hosts='192.168.1.0/24', arguments='-n -sP -PE -PA21,23,80,3389')
            hosts_list = [(x, nm[x]['status']['state']) for x in nm.all_hosts() if nm[x]['status']['state'] == 'up']

            for host, status in hosts_list:
                complet.append(f"Port scan results for {host}:")
                
                try:
                    nm.scan(hosts=host, arguments='-n -p 1-100')
                    
                    # Affichez les ports actifs pour chaque machine
                    for port, port_info in nm[host]['tcp'].items():
                        if port_info['state'] == 'open':
                            complet.append(f"Port {port} is open ({port_info['name']})")

                except Exception as e:
                    complet.append(f"Aucun port ouvert")
                
                complet.append("") 

            count_machine = len(hosts_list)
                    
            complet = '\n'.join(complet)    
        except Exception as e:
            print("Une erreur s'est produite :", str(e))
            count_machine = "ERROR"

 
        try:
            # Utilisez la commande ping pour mesurer la latence
            cmd = ['ping', '-c', '4', '8.8.8.8']
            result = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True, check=True)
            
            # Si aucune erreur ne s'est produite
            if not result.stderr:
                # Convertir la sortie binaire en chaîne
                output_str = result.stdout
                
                # Utiliser une expression régulière pour extraire les temps de réponse du ping
                rtt_times = re.findall(r'time=(\d+\.\d+) ms', output_str)

                if rtt_times:
                    # Convertir les temps de réponse en entiers
                    rtt_times = [float(time) for time in rtt_times]
                    
                    # Calculer la moyenne des temps de réponse
                    average_rtt = sum(rtt_times) / len(rtt_times)
                    
                    print(f"{average_rtt} ms")
                    rtt = average_rtt
                else:
                    print("Aucun temps de réponse trouvé.")
                    rtt = "Aucun temps de réponse trouvé"

            else:
                rtt = ("ERROR","Erreurs lors de l'exécution de la commande ping:",{result.stderr})

        except subprocess.CalledProcessError as e:
            print(f"Une erreur s'est produite :", str(e))
            rtt = ("ERROR",str(e))
        except Exception as e:
            print("Une erreur inattendue s'est produite :", str(e))
            rtt = ("ERROR",str(e))

    return render_template('index.html', local_ip=local_ip, vm_name=vm_name, count_machine=count_machine, rtt=rtt, complet = complet)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)
